env = ./.env.example

ifneq ("$(wildcard ./.env)","")
    env = ./.env
endif

docker = docker-compose -f ./docker/docker-compose.yml -f ./docker/docker-compose.override.yml --env-file ${env}

include ${env}
export

# Запустить установку проекта
.PHONY: install
install:
	echo ${env}
	cp docker/example.docker-compose.override.yml docker/docker-compose.override.yml
	cp .env.example .env
	${docker} up -d
	${docker} exec -i postgres psql -U ${DB_USERNAME} -d ${DB_DATABASE} -c "CREATE EXTENSION postgis;"
	${docker} exec php-fpm sh -c "composer update"
	${docker} exec php-fpm sh -c "php artisan key:generate"
	${docker} exec php-fpm sh -c "php artisan storage:link"
	${docker} exec php-fpm sh -c "php artisan migrate:fresh"
	${docker} exec php-fpm sh -c "php artisan db:seed"
	${docker} exec php-fpm sh -c "php artisan voyager:admin admin@rocketfirm.net --create"

# Обновление тестового  сервера
.PHONY: update-code-dev
update-code-dev:
	@composer install --ignore-platform-reqs
	/opt/plesk/php/7.4/bin/php artisan migrate
	/opt/plesk/php/7.4/bin/php artisan db:seed

# Запустить Docker демона
.PHONY: run
run:
	${docker} up -d
	${docker} exec php-fpm sh -c "composer install"
	${docker} exec php-fpm sh -c "php artisan migrate"
	${docker} exec php-fpm sh -c "php artisan db:seed"

# Остановить работу Docker'а
.PHONY: stop
stop:
	${docker} stop

# Зайти в bash php-fpm
.PHONY: php
php:
	${docker} exec php-fpm bash -l

# Зайти в sh mariadb
.PHONY: mariadb
mariadb:
	${docker} exec mariadb sh -l

# Зайти в sh postgres
.PHONY: postgmariadbres
postgres:
	${docker} exec postgres bash -l

# Зайти в sh redis
.PHONY: redis
redis:
	${docker} exec redis sh -l

# Зайти в sh nginx
.PHONY: nginx
nginx:
	${docker} exec nginx sh -l

# Зайти в sh mongo
.PHONY: mongo
mongo:
	${docker} exec mongo sh -l

# Зайти в sh adminer
.PHONY: adminer
adminer:
	${docker} exec adminer sh -l

# Зайти в bash mongo-express
.PHONY: mongo-express
mongo-express:
	${docker} exec mongo-express bash -l

# Зайти в bash elasticsearch
.PHONY: elasticsearch
elasticsearch:
	${docker} exec elasticsearch bash -l

